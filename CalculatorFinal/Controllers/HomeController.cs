﻿using CalculatorFinal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CalculatorFinal.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        private readonly IProcessor _pro;

        public  HomeController(IProcessor pro)
        {
            _pro=pro;
        }
        [HttpGet]
        public ActionResult Index()
        {
            return View(new Input());
        }

    

        [HttpPost]
        public ActionResult result(Input obj1, string sign)
        {
            int k = _pro.CalculateMethod(obj1.ip1, obj1.ip2, sign);
            return Content(string.Format("{0}",k));

        }
    }
}