﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CalculatorFinal.Models
{
    public class CalculateDiv:ICalculate
    {
        public bool CanCalculate(string sign)
        {
            return sign == "div";
        }
        public int Calculate(int ip1, int ip2)
        {
            return ip1 / ip2;
        }
    }
}