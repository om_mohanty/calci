﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorFinal.Models
{
    public interface ICalculate
    {
        bool CanCalculate(string sign);
        int Calculate(int ip1, int ip2);
    }
}
