﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CalculatorFinal.Models
{
    public class Processor : IProcessor
    {
        private  IList<ICalculate> _calculators;
        public Processor(IList<ICalculate> calculators)
        {
            /*this._calculators.Add(new CalculateAdd());
            this._calculators.Add(new CalculateSub());
            this._calculators.Add(new CalculateMul());
            this._calculators.Add(new CalculateDiv());*/
            _calculators = calculators;

        }
        public int CalculateMethod(int ip1, int ip2, string sign)
        {
            var calculator = this._calculators.FirstOrDefault(x => x.CanCalculate(sign));
            return calculator.Calculate(ip1, ip2);
        }
    }
}