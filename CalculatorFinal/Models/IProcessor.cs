﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorFinal.Models
{
    public interface IProcessor
    {
        int CalculateMethod(int ip1, int ip2, string sign);
    }
}
