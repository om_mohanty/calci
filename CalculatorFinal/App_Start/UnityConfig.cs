using CalculatorFinal.Controllers;
using CalculatorFinal.Models;
using System.Web.Mvc;
using Unity;
using Unity.Injection;
using Unity.Mvc5;

namespace CalculatorFinal
{
	public static class UnityConfig
	{
		public static void RegisterComponents()
		{
			var container = new UnityContainer();

			// register all your components with the container here
			// it is NOT necessary to register your controllers
			

			container.RegisterType<IProcessor, Processor>();
			container.RegisterType<ICalculate, CalculateAdd>();
			container.RegisterType<ICalculate, CalculateSub>();
			container.RegisterType<ICalculate, CalculateMul>();
			container.RegisterType<ICalculate, CalculateDiv>();

			DependencyResolver.SetResolver(new UnityDependencyResolver(container));
		}
	}
}