﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CalculatorFinal.Models
{
    public class CalculateMul:ICalculate
    {
        public bool CanCalculate(string sign)
        {
            return sign == "mul";
        }
        public int Calculate(int ip1, int ip2)
        {
            return ip1 * ip2;
        }
    }
}